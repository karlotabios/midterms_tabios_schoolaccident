<?php

use Plugins\SchoolAccident\SchoolAccidentModel as SchoolAccident;
use Modules\Helathcareservices\Entities\Healthcareservices;
use Shine\Repositories\Eloquent\HealthcareRepository as HealthcareRepository;
use Shine\Repositories\Contracts\HealthcareRepositoryInterface;
use Shine\Http\Controllers\Controller;
use Shine\Libraries\IdGenerator;
use Shine\User;
use Shine\Plugin;

class SchoolAccidentController extends Controller
{
    protected $moduleName = 'Healthcareservices';
    protected $modulePath = 'healthcareservices';

    public function __construct(HealthcareRepository $healthcareRepository) {
        $this->healthcareRepository = $healthcareRepository;
        $this->middleware('auth');
    }

    public function index()
    {
        //no index
    }

    public function save($data)
    {
        $schoolaccident_id = $data['fpservice_id'];
        $hservice_id = $data['hservice_id'];
        $patient_id = $data['patient_id'];

        $schoolaccident_service = SchoolAccident::where('schoolaccident_id','=',$schoolaccident_id)->first();

        if($schoolaccident_service == NULL) {
            $schoolaccident_service =  new SchoolAccident;
            $schoolaccident_service->healthcareservice_id = $hservice_id;
            $schoolaccident_service->patient_id = $patient_id;
        }

        $schoolaccident_service->save();
        header('Location: '.site_url().'healthcareservices/edit/'.$patient_id.'/'.$hservice_id);

        exit;
    }
}
?>