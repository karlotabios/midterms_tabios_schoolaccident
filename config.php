<?php
$plugin_id = 'SchoolAccident';                       //plugin ID
$plugin_module = 'healthcareservices';          //module owner
$plugin_location = 'dropdown';                  //UI location where plugin will be accessible
$plugin_primaryKey = 'schoolaccident_id';        //primary_key used to find data
$plugin_table = 'schoolaccident_service';            //plugintable default; table_name custom table
$plugin_tabs_child = array('addservice', 'complaints', 'vitals', 'schoolaccident_plugin', 'impanddiag', 'medicalorders', 'disposition'); //,
$plugin_type = 'program';
$plugin_gender = "all";
$plugin_age = "5-30";
//plugin maximum role to access this plugin (MANDATORY VALUE)
$plugin_role = 5;

$plugin_relationship = array();
$plugin_folder = 'SchoolAccident'; //module owner
$plugin_title = 'School Accident';            //plugin title
$plugin_description = 'My School Accident';
$plugin_version = '1.0';
$plugin_developer = 'Karlo';
$plugin_url = 'http://www.shine.ph';
$plugin_copy = "2018";

$plugin_tabs = [
    'addservice' => 'Basic Information',
    'complaints' => 'Complaints',
    'impanddiag' => 'Impressions & Diagnosis',
    'disposition' => 'Disposition',
    'medicalorders' => 'Medical Orders',
    'vitals' => 'Vitals & Physical',
    'schoolaccident_plugin' => 'School Accident'
];

$plugin_tabs_models = [
    'complaints' => 'GeneralConsultation',
    'disposition' => 'Disposition',
    'medicalorders' => 'MedicalOrder',
    'vitals' => 'VitalsPhysical',
    'schoolaccident_plugin' => 'SchoolAccidentModel'
];