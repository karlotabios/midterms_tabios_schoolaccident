<?php
    $hservice_id = $allData['healthcareserviceid'];
    $schoolaccident_id = $data['schoolaccident_id'];
    $patient_id = $allData['patient']->patient_id;
    $currentdate = date("m/d/Y");
    $is_PE_related = false;
    //let us disable this form if this consult is already disposed
    if(empty($allData['disposition_record']['disposition'])) {
       $read = '';
     }
    else {
      $read = 'disabled';
      if($data['PE_related'] == 1) {
        $is_PE_related = true;
      }
    }
?>
<!-- name is plugin id -->
{!! Form::hidden('myhealthcareplugin[fpservice_id]',$schoolaccident_id) !!}
{!! Form::hidden('myhealthcareplugin[hservice_id]',$hservice_id) !!}
{!! Form::hidden('myhealthcareplugin[patient_id]',$patient_id) !!}
<legend>Mental Status Examination Form</legend>

<!-- First Textfield -->
<fieldset>
    <div class="form-group">
        <label class="col-md-2">Kind of Injury</label>
        <div class="col-md-10">
          {!! Form::text('myhealthcareplugin[kind_of_injury]', $data['kind_of_injury'], ['class' => 'form-control', 'placeholder' => 'Friendzone', $read]) !!}
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-2">Cause of Injury</label>
        <div class="col-md-10">
          {!! Form::text('myhealthcareplugin[cause_of_injury]', $data['cause_of_injury'], ['class' => 'form-control', 'placeholder' => 'Si Crush', $read]) !!}
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-2">Degree of Injury</label>
        <div class="col-md-10">
          {!! Form::text('myhealthcareplugin[degree_of_injury]', $data['degree_of_injury'], ['class' => 'form-control', 'placeholder' => 'Ayoko na Magmahal Muli', $read]) !!}
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-2">Kind of Injury</label>
        <div class="col-md-10">
          {!! Form::text('myhealthcareplugin[kind_of_injury]', $data['kind_of_injury'], ['class' => 'form-control', 'placeholder' => 'Brokenhearted', $read]) !!}
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-2">Parts of Body Injured</label>
        <div class="col-md-10">
          {!! Form::text('myhealthcareplugin[parts_of_body_injured]', $data['parts_of_body_injured'], ['class' => 'form-control', 'placeholder' => 'Whole Body', $read]) !!}
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-2">Immediate Action Taken</label>
        <div class="col-md-10">
          {!! Form::text('myhealthcareplugin[immediate_action_taken]', $data['immediate_action_taken'], ['class' => 'form-control', 'placeholder' => 'Maglasing', $read]) !!}
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-2">Location of Accident</label>
        <div class="col-md-10">
          {!! Form::text('myhealthcareplugin[location_of_accident]', $data['location_of_accident'], ['class' => 'form-control', 'placeholder' => 'Sa Ateneo', $read]) !!}
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-2">Activity of Student</label>
        <div class="col-md-10">
          {!! Form::text('myhealthcareplugin[acitivity_of_student]', $data['acitivity_of_student'], ['class' => 'form-control', 'placeholder' => 'Pagmamahal', $read]) !!}
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-2">PE related</label>
        <div class="col-md-10">
          Yes {!! Form::radio('myhealthcareplugin[PE_related]', $data['PE_related'], ['class' => 'form-control', 'placeholder' => 'Yes', $read], $is_PE_related) !!}
          No {!! Form::radio('myhealthcareplugin[PE_related]', $data['PE_related'], ['class' => 'form-control', 'placeholder' => 'No', $read], $is_PE_related) !!}
        </div>
    </div>
</fieldset>