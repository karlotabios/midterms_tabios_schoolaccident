<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMigrationSchoolAccident extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    
    public function up()
    {
        if (Schema::hasTable('schoolaccident_service')!=TRUE) { 
            Schema::create('schoolaccident_service', function (Blueprint $table) {
                $table->increments('id');
                $table->string('schoolaccident_id', 60);
                $table->string('patient_id', 60);
                $table->string('healthcareservice_id', 60);
                $table->string('kind_of_injury',60);
                $table->string('cause_of_injury',60);
                $table->string('degree_of_injury',60);
                $table->string('parts_of_body_injured',60);
                $table->string('immediate_action_taken',60);
                $table->string('location_of_accident',60);
                $table->string('activity_of_student',60);
                $table->integer('PE_related');
                $table->text('description_of_accident');
                


                // a. KindofInjury(string)
                // b. CauseofInjury(string)
                // c. Degree of Injury (string)
                // d. Parts of Body Injured (string)
                // e. Immediate Action T aken (string)
                // f. Location of Accident (string)
                // g. ActivityofStudent(string)
                // h. PE related (integer, either 0 for No or 1 for Yes)
                // i. Description of Accident (text)

                $table->softDeletes();
                $table->timestamps();
                $table->unique('healthcareservice_id');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('mentalhealth_service');
    }
}